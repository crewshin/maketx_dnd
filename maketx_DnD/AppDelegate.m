//
//  AppDelegate.m
//  maketx_DnD
//
//  Created by Gene Crucean on 1/23/13.
//  Copyright (c) 2013 Gene Crucean. All rights reserved.
//

#import "AppDelegate.h"
#import "GCConstants.h"
#import "QuartzCore/QuartzCore.h"

#define kMakeTXPath @"maktTXPath"
#define kFlagsToPass @"flagsToPass"
#define kCloseAfterConversion @"closeAfterConversion"

@implementation AppDelegate
@synthesize mainWindow;

#pragma mark - Stock Apple

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    LOG3 ? NSLog(@"applicationDidFinishLaunching") : nil;
    
    [self setupGUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(readPipe:)
                                                 name:NSFileHandleReadCompletionNotification
                                               object:nil];
}

- (void)setupGUI
{
    [_pathToMakeTXTextField setStringValue:[[NSUserDefaults standardUserDefaults] stringForKey:kMakeTXPath]];
    [_flagsToPassTextField setStringValue:[[NSUserDefaults standardUserDefaults] stringForKey:kFlagsToPass]];
    [_closeAfterConversionButton setState:[[NSUserDefaults standardUserDefaults] boolForKey:kCloseAfterConversion]];
}


#pragma mark - PROCESS FILES

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (BOOL)copyFileToUserDestiniation
{
    NSError *error;
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    [fileManager copyItemAtPath:pathSandboxed toPath:saveToPath error:&error];
    return YES;
}


- (void)application:(NSApplication *)theApplication openFiles:(NSArray *)filenames
{
    LOG3 ? NSLog(@"openFiles") : nil;
    
    if (isWindowless)
    {
        [_window setIsVisible:NO];
    }
    else
    {
        [_window setIsVisible:YES];
    }
    
    if (filenames.count == 0)
    {
//        [outputStatusLabel setStringValue:@"Application is designed to have files dropped onto the icon in the dock."];
        LOG5 ? NSLog(@"////////// WARNING: App is meant to drag files onto it's icon.") : nil;
    }
    else
    {
        LOG5 ? NSLog(@"NSUSerDefaults:makeTXPath: %@", [[NSUserDefaults standardUserDefaults] stringForKey:kMakeTXPath]) : nil;
//        LOG5 ? NSLog(@"filenames: %@", filenames) : nil;
        
        passedFiles = [[NSArray alloc] initWithArray:filenames];
        [self processFiles:filenames];
    }
}


- (BOOL)processFiles:(NSArray *)files
{
    NSLog(@"The following file has been dropped or selected: %@", files);
    
    for (int i = 0; i < files.count; i++)
    {
        // Create NSTask.
        task = [[NSTask alloc] init];
        
        nImages = i + 1;
        [_outputLabel setTextColor:[NSColor colorWithDeviceRed:0.0 green:0.0 blue:0.0 alpha:1.0]]; // BLACK
        [_outputLabel setStringValue:[NSString stringWithFormat:@"%d images are being converted.", nImages]];
        
        // Build path to maketx.
    //    NSFileManager *fileManager = [[NSFileManager alloc] init];
    //    
    //    // Make sure maketx binary is in the proper location.
    //    if ([fileManager fileExistsAtPath:maketxPath]) // maketx exists.
    //    {
    //        LOG3 ? NSLog(@"maketx binary exists!!!") : nil;
    //        LOG5 ? NSLog(@"//////////////// launchPath: %@", maketxPath) : nil;
    //        
    //        [task setLaunchPath:maketxPath];
    //    }

        LOG3 ? NSLog(@"maketx binary exists!!!") : nil;
        LOG5 ? NSLog(@"//////////////// launchPath: %@", [_pathToMakeTXTextField stringValue]) : nil;
        
        if (![_pathToMakeTXTextField stringValue] || [[_pathToMakeTXTextField stringValue] isEqualToString:@""])
        {
            if ([[NSUserDefaults standardUserDefaults] stringForKey:kMakeTXPath])
            {
                [task setLaunchPath:[[NSUserDefaults standardUserDefaults] stringForKey:kMakeTXPath]];
            }
            else
            {
                [_outputLabel setTextColor:[NSColor colorWithDeviceRed:0.8 green:0.1 blue:0.1 alpha:1.0]]; // RED
                [_outputLabel setStringValue:@"Please set the path to the maketx binary and try again."];
                return NO;
            }
        }
        else
        {
            [task setLaunchPath:[_pathToMakeTXTextField stringValue]];
        }
        
        // Make sure there are flags typed into the textfield.
        if ([_flagsToPassTextField stringValue] != nil)
        {
            flagsToPass = [[NSMutableArray alloc] initWithArray:[[_flagsToPassTextField stringValue] componentsSeparatedByString:@" "]];
        }
        
        [flagsToPass addObject:@"-v"];
        
        LOG5 ? NSLog(@"============ flagsToPass: %@", flagsToPass) : nil;
        
        // Add input files to end of arguments array.
        [flagsToPass addObject:[passedFiles objectAtIndex:i]];
        
        NSPipe *pipe = [[NSPipe alloc] init];
        fileHandle = [pipe fileHandleForReading];
        [fileHandle readInBackgroundAndNotify];
        
        [task setStandardOutput:pipe];
        [task setStandardError:pipe];
        
//        LOG5 ? NSLog(@"filesOutPaths: %@", fileOutPaths) : nil;
        
        [task setArguments:flagsToPass];
        
        [task launch];
        
//        if ([[[_pathToMakeTXTextField stringValue] lastPathComponent] isEqualToString:@"maketx"])
//        {
//            [task launch];
//        }
//        else
//        {
//            [_outputLabel setTextColor:[NSColor colorWithDeviceRed:0.8 green:0.1 blue:0.1 alpha:1.0]]; // RED
//            [_outputLabel setStringValue:@"Please check that the maketx path is correct."];
//            return NO;
//        }
    }
    
    taskTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkIfTaskIsFinished) userInfo:nil repeats:YES];
    
    return YES;
}


- (void)checkIfTaskIsFinished
{
    if (![task isRunning])
    {
        NSSound *sound;
        sound = [[NSSound alloc] initWithContentsOfFile:@"/System/Library/Sounds/Glass.aiff" byReference:YES];
        [sound play];
        [taskTimer invalidate];
        [_outputLabel setTextColor:[NSColor colorWithDeviceRed:0.0 green:0.0 blue:0.0 alpha:1.0]]; // BLACK
        [_outputLabel setStringValue:[NSString stringWithFormat:@"%d images have been converted.", nImages]];
        
        [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(clearOutputLabel) userInfo:nil repeats:NO];
    }
}


- (void)clearOutputLabel
{
    [_outputLabel setTextColor:[NSColor colorWithDeviceRed:0.0 green:0.0 blue:0.0 alpha:1.0]]; // BLACK
    [_outputLabel setStringValue:@""];
    
    if ([_closeAfterConversionButton state])
    {
        [self exitApp];
    }
}


- (void)goodTimeTo
{
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(clearOutputLabel) userInfo:nil repeats:NO];
    
    [_outputLabel setTextColor:[NSColor colorWithDeviceRed:0.0 green:0.0 blue:0.0 alpha:1.0]]; // BLACK
    [_outputLabel setStringValue:@"Actually now might be a good time to click 'Save current state'"];
}

- (void)readPipe:(NSNotification *)notification
{
    NSData *data;
    NSString *text;
    
    if( [notification object] != fileHandle )
    {
        return;
    }
    
    data = [[notification userInfo] objectForKey:NSFileHandleNotificationDataItem];
    text = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    
    NSScroller *scroller = [[_nsTaskOutput enclosingScrollView] verticalScroller];
    BOOL shouldScrollToBottom = [scroller doubleValue] == 1.0;
    NSTextStorage *ts = [_nsTaskOutput textStorage];
    [ts replaceCharactersInRange:NSMakeRange([ts length], 0) withString:text];
    if (shouldScrollToBottom)
    {
        NSRect bounds = [_nsTaskOutput bounds];
        [_nsTaskOutput scrollPoint:NSMakePoint(NSMaxX(bounds), NSMaxY(bounds))];
    }
    
    if (data != 0)
    {
        [fileHandle readInBackgroundAndNotify];
    }
}


- (void)exitApp
{
    exit(0); // BOOSH!! See ya!
}



#pragma mark - IBACTIONS

/////////////////////////////////////////////////////////////////////////////
// File Open Dialog
/////////////////////////////////////////////////////////////////////////////
- (IBAction)fileOpenClick:(id)sender
{
//    LOG3 ? NSLog(@"~~>>%@ :%@", self.nibName, NSStringFromSelector(_cmd)) : nil;
    
    NSOpenPanel *openDlg = [NSOpenPanel openPanel];
    
    [openDlg setCanChooseFiles:YES];
    [openDlg setCanChooseDirectories:NO];
    [openDlg setCanCreateDirectories:YES];
    
    if ([openDlg runModal] == NSOKButton)
    {
        NSArray *files = [openDlg URLs];
        
        LOG5 ? NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ files: %@", files) : nil;
        
        maketxPath = [[files objectAtIndex:0] path];
        NSLog(@"//////// fileName ////////// %@", maketxPath);
        
        if ([sender tag] == 2) // maketx binary path.
        {
            // Set path to gui.
            [_pathToMakeTXTextField setStringValue:maketxPath];
            [_outputLabel setTextColor:[NSColor colorWithDeviceRed:0.0 green:0.0 blue:0.0 alpha:1.0]]; // BLACK
            [_outputLabel setStringValue:@"GET TO THE CHOPPAAA!!"];
            
            [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(goodTimeTo) userInfo:nil repeats:NO];
        }
    }
}

- (IBAction)saveCurrentState:(id)sender
{
    LOG5 ? NSLog(@"saveCurrentState:") : nil;
    // Set path to prefs.
    [[NSUserDefaults standardUserDefaults] setValue:[_pathToMakeTXTextField stringValue] forKey:kMakeTXPath];
    [[NSUserDefaults standardUserDefaults] setValue:[_flagsToPassTextField stringValue] forKey:kFlagsToPass];
    [[NSUserDefaults standardUserDefaults] setBool:[_closeAfterConversionButton state] forKey:kCloseAfterConversion];
}

- (IBAction)resetToDefaultsButton:(id)sender
{
    [_flagsToPassTextField setStringValue:@"--filter lanczos3 --oiio"];
//    [_pathToMakeTXTextField setStringValue:@""];
    
    [self saveCurrentState:nil];
}



#pragma mark - Dealloc

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
