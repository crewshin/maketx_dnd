//
//  AppDelegate.h
//  maketx_DnD
//
//  Created by Gene Crucean on 1/23/13.
//  Copyright (c) 2013 Gene Crucean. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreData/CoreData.h>


@interface AppDelegate : NSObject <NSApplicationDelegate, NSTextFieldDelegate>
{
    NSTask *task;
    NSWindowController *mainWindow;
    BOOL didPlaySound;
    NSString *maketxPath;
    NSMutableArray *flagsToPass;
    NSString *saveToPath;
    NSArray *passedFiles;
    NSString *pathSandboxed;
    NSFileHandle *fileHandle;
    NSTimer *taskTimer;
    int nImages;
}


@property (assign) IBOutlet NSWindow *window;
@property (nonatomic, retain) NSWindowController *mainWindow;
@property (weak) IBOutlet NSTextField *pathToMakeTXTextField;
@property (weak) IBOutlet NSTextField *flagsToPassTextField;
@property (assign) IBOutlet NSTextView *nsTaskOutput;
@property (weak) IBOutlet NSTextField *outputLabel;
@property (weak) IBOutlet NSButton *closeAfterConversionButton;


- (BOOL)processFiles:(NSArray *)files;
- (IBAction)fileOpenClick:(id)sender;
- (IBAction)saveCurrentState:(id)sender;
- (IBAction)resetToDefaultsButton:(id)sender;
- (void)readPipe:(NSNotification *)notification;




@end
