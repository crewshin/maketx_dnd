//
//  main.m
//  maketx_DnD
//
//  Created by Gene Crucean on 1/23/13.
//  Copyright (c) 2013 Gene Crucean. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
